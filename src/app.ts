import 'phaser'

class GameApplication
{
  game: Phaser.Game;

  constructor ()
  {
    this.game = new Phaser.Game();
  }
}

window.onload = () => new GameApplication();